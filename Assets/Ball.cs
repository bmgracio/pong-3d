﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

    public Vector3 initialPulse;

	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody>().AddForce(initialPulse, ForceMode.Impulse);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
