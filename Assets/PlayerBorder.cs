﻿using UnityEngine;
using System.Collections;

public class PlayerBorder : MonoBehaviour {

    public ePlayer player;
    public ScoreUi score;

    void OnCollisionEnter(Collision col) {
        Ball ball = col.gameObject.GetComponent<Ball>();

        if (ball != null) {
            ball.transform.position = new Vector3(0f, 1f, 0f);

            if (player == ePlayer.Player1) {
                score.scorePlayer2++;
            }

            if (player == ePlayer.Player2) {
                score.scorePlayer1++;
            }
        }
    }

}
