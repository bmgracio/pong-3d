﻿using UnityEngine;
using System.Collections;

public enum ePlayer { Player1, Player2 }

public class Player : MonoBehaviour {

    public float speed = 15f;
    public ePlayer player;

	// Use this for initialization
	void Start () {
	    //we don't need this one
	}
	
	// Update is called once per frame
	void Update () {
        float inputSpeed = 0f;

        if (player == ePlayer.Player1) {
            inputSpeed = Input.GetAxisRaw("Player 1");
        }

        if (player == ePlayer.Player2) {
            inputSpeed = Input.GetAxisRaw("Player 2");
        }

        transform.position += new Vector3(0f, 0f, inputSpeed * speed * Time.deltaTime);
	}
}
